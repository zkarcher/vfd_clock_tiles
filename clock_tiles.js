const fs = require("fs");
const readImage = require("readimage");
const SerialPort = require("serialport");
const _ = require("underscore");
const TweenMax = require("gsap");

const CHARS_ACROSS = 20;
const CHARS_DOWN = 2;
const CHARS_TOTAL = CHARS_ACROSS * CHARS_DOWN;

const INITIAL_CODEPOINT = 0x21;

const COMMAND_CLEAR_DISPLAY = "\x0C";
const COMMAND_GO_HOME = "\x0B";
const COMMAND_GO_BOTTOM_LEFT = "\x1F\x24\x01\x02";

const FONT_SHAPE_FILE = "art/font_2.png";

const DOT_COUNT = 4;
const DOT_FRAMES = 6;
const DOT_X_LEFT = 6;
const DOT_X_RIGHT = 13;

const NO_GLYPH = -1;
const A_STAR_NOT_FILLED = 127;

// https://stackoverflow.com/questions/1431094/how-do-i-replace-a-character-at-a-particular-index-in-javascript
String.prototype.replaceAt = function(index, replacement) {
	return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
}

function lerp(a, b, prog) {
	return a + (b - a) * prog;
}

function rand(top) {
	return Math.random() * top;
}

function randRange(bottom, top) {
	return lerp(bottom, top, Math.random());
}

var port;
var imgMap = [];	// {.x, .y, .codepoint, .bytes}
var digitFeatures = [];	// [feature strings x 4]  // Digit font
var aStarGrid = [];

var cur_frame = "";
var new_frame = "";
var cursor_pos = 0;
var dest_features = [];	// Array of strings, "(tr)_b" etc, one for each char
var tween_codepoints = [];	// Array of arrays...
var tween_objs = [];

var tweenDots = [];	// 4 blinky dots

//
//  Init data
//

var spaceCoords = featureNameToCoords(" ")[0];
var spaceCodepoint = coordsToCodepoint(spaceCoords[0], spaceCoords[1]);
for (var c = 0; c < CHARS_TOTAL; c++) {
	dest_features.push(" ");
	tween_codepoints.push([spaceCodepoint]);
	cur_frame += chr(spaceCodepoint);
	new_frame += chr(spaceCodepoint);
	tween_objs.push({v: 0.0});
}
for (var d = 0; d < DOT_COUNT; d++) {
	tweenDots.push({v: 0.0});
}

function log2d(arr) {
	console.log("=================================");
	for (var j = 0; j < arr[0].length; j++) {
		var items = [];
		for (var i = 0; i < arr.length; i++) {
			items.push(("       " + arr[i][j]).substr(-4));
		}
		console.log(items.join(","))
	}
	console.log("---------------------------------");
}

function tweenCharUpdate(idx) {
	var prog_i = Math.round(tween_objs[idx].v);
	var codepoint = tween_codepoints[idx][prog_i];
	new_frame = new_frame.replaceAt(idx, chr(codepoint));

	//console.log("ZKA:", tween_objs[idx].v);
}

function tweenDotUpdate(idx) {
	var prog_i = Math.round(tweenDots[idx].v) % DOT_FRAMES;
	var codepoint = INITIAL_CODEPOINT + prog_i;
	var ch = ((idx & 0x1) ? DOT_X_RIGHT : DOT_X_LEFT) + ((idx & 0x2) ? CHARS_ACROSS : 0);
	new_frame = new_frame.replaceAt(ch, chr(codepoint));
}

function tweenDotComplete(idx) {
	// Restart dot animation. Always keep .v positive.
	tweenDots[idx].v = (tweenDots[idx].v % DOT_FRAMES);

	//var dir = (rand(1.0) < 0.5) ? -1 : 1;
	var dir = 1;
	var time = randRange(5.0, 20.0);
	var distance = time * 10;

	var dest;
	if (dir < 0) {
		tweenDots[idx].v += Math.ceil(distance);
		dest = tweenDots[idx].v - distance;
	} else {
		dest = tweenDots[idx].v + distance;
	}

	TweenMax.to(
		tweenDots[idx],
		time,
		{v: dest, ease: Sine.easeInOut, onUpdate: tweenDotUpdate, onUpdateParams: [idx], onComplete: tweenDotComplete, onCompleteParams: [idx]}
	);
}

function getAStarGrid() {
	// Clear aStarGrid: Reset non-negative values to 0
	for (var i = 0; i < aStarGrid.length; i++) {
		for (var j = 0; j < aStarGrid[i].length; j++) {
			if (aStarGrid[i][j] !== NO_GLYPH) aStarGrid[i][j] = A_STAR_NOT_FILLED;
		}
	}

	return aStarGrid;
}

function coordsToCodepoint(x, y) {
	var obj = _.find(imgMap, function(obj){
		return (obj.x == x) && (obj.y == y);
	}) || {};

	return obj["codepoint"] || " ".charCodeAt(0);
}

// Returns 2D array of possible locations to obtain this feature.
function featureNameToCoords(name) {
	var pts = [[-1, -1]];

	// Names: 't'==top, 'b'==bottom, 'l'==left, 'r'==right
	switch (name) {
		// Straight line sides
		case "l":              pts = [[12, 14]]; break;
		case "r":              pts = [[2, 26]]; break;
		case "t":              pts = [[5, 19]]; break;
		case "b":              pts = [[14, 16], [0, 28]]; break;

		// Square corners
		case "tl":             pts = [[14, 28]]; break;
		case "tr":             pts = [[7, 19]]; break;
		case "bl":             pts = [[5, 28]]; break;
		case "br":             pts = [[2, 28]]; break;

		// Boxes
		case "[":              pts = [[5, 14]]; break;
		case "]":              pts = [[9, 14]]; break;
		case "=":              pts = [[7, 14]]; break;

		// Curved corners
		case "(tl)":           pts = [[0, 14]]; break;
		case "(tr)":           pts = [[12, 7]]; break;
		case "(bl)":           pts = [[19, 16]]; break;
		case "(br)":           pts = [[9, 28]]; break;
		case "(tl)_b":         pts = [[5, 9]]; break;

		// Special
		case "z":              pts = [[12, 21]]; break;
		case " ":              pts = [[24, 16], [0, 26], [14, 14]]; break;

		case ".":              pts = [[0, 0]]; break;

		default:
			console.log("** feature not found:", name);
			break;
	}

	return pts;
}

function codepointsForAnimationBetweenCoords(fromCoords, toCoords) {
	var grid = getAStarGrid();

	// Starting at fromCoords: Fill outwards, find shortest path to toCoords.
	var fillNodes = [fromCoords];
	grid[fromCoords[0]][fromCoords[1]] = 0;

	// Optimization: When we reach toCoords, store the best value (shortest distance).
	var bestValue = A_STAR_NOT_FILLED;

	while (fillNodes.length) {
		var node = fillNodes.shift();
		var x = node[0];
		var y = node[1];
		var value = grid[x][y];

		// Optimization: If this path exceeds known bestValue: it's too long. Die.
		if (value >= bestValue) continue;

		function tryAddNode(newX, newY) {
			if ((newX < 0) || (grid.length <= newX)) return;
			if ((newY < 0) || (grid[0].length <= newY)) return;
			if (grid[newX][newY] == NO_GLYPH) return;

			var newValue = value + 1;

			// This path would be backtracking, or already known as a faster route? Bail.
			if (grid[newX][newY] <= newValue) return;

			// OK to keep searching here.
			grid[newX][newY] = newValue;

			// Did we reach the destination (toCoords)?
			if ((newX == toCoords[0]) && (newY == toCoords[1])) {
				bestValue = Math.min(bestValue, newValue);

			} else {
				// Keep searching...
				fillNodes.push([newX, newY]);
			}
		}

		tryAddNode(x - 1, y);
		tryAddNode(x + 1, y);
		tryAddNode(x, y - 1);
		tryAddNode(x, y + 1);

	}	// !while (fillNodes.length)

	// Now we have calculated the shortest path between two coordinates on the map.
	// Backtrack from toCoords, back to fromCoords, descending values.
	// Store the codepoints for this journey. This will be our animated sequence.
	var codepoint_ar = [];
	var currentValue = bestValue;
	var coords = toCoords.slice(0);	// clone

	for (var walk = 0; walk < 30; walk++) {	// safer than while()
		codepoint_ar.unshift(coordsToCodepoint(coords[0], coords[1]));

		function tryWalkBackwards(newX, newY) {
			if ((newX < 0) || (grid.length <= newX)) return;
			if ((newY < 0) || (grid[0].length <= newY)) return;
			if (grid[newX][newY] == NO_GLYPH) return;

			// Don't walk uphill (descending values only)
			var newValue = grid[newX][newY];
			if (newValue >= currentValue) return;

			//console.log(coords, "MOVE TO:", newX, newY, "->", newValue);
			currentValue = newValue;
			coords = [newX, newY];
			return true;
		}

		var x = coords[0];
		var y = coords[1];
		if (tryWalkBackwards(x - 1, y)) continue;
		if (tryWalkBackwards(x + 1, y)) continue;
		if (tryWalkBackwards(x, y - 1)) continue;
		if (tryWalkBackwards(x, y + 1)) continue;

		// No more movement is possible
		break;

	}	// for (var walk...)

	return codepoint_ar;
}

function codepointsForAnimation(fromName, toName){
	var fromCoordsAr = featureNameToCoords(fromName);
	var toCoordsAr = featureNameToCoords(toName);

	var shortestPath = null;

	for (var from = 0; from < fromCoordsAr.length; from++) {
		for (var to = 0; to < toCoordsAr.length; to++) {
			var path = codepointsForAnimationBetweenCoords(fromCoordsAr[from], toCoordsAr[to]);
			if (!shortestPath || (path.length < shortestPath.length)) {
				shortestPath = path;
			}
		}
	}

	return shortestPath;
}

function defineDigit(n, feature_ar) {
	digitFeatures[n] = feature_ar;
}

function debugStr(str) {
	var out = [];
	for (var i = 0; i < str.length; i++) {
		out.push(ord(str.substr(i,1)).toString(16));
	}
	console.log(out.join(","));
}

function chr(n) {
	return String.fromCharCode(n);
}

function ord(str) {
	return str.charCodeAt(0);
}

// n: 1..4
function setBrightness(n) {
	port.write("\x1F\x58" + chr(n));
}

function clearDisplay() {
	port.write(COMMAND_CLEAR_DISPLAY);
}

function cursorGoHome() {
	port.write(COMMAND_GO_HOME);
}

function codepointAtMapLoc(x, y) {
	var found = _.find(imgMap, function(obj){
		return (obj.x == x) && (obj.y == y);
	}) || {};

	return found.codepoint;
}

// The manual calls these "download characters"
function enableCustomCharacters() {
	//port.write("\x1B\x74\x01");	// Katakana
	//port.write("\x1B\x74\x00");	// Euro
	port.write("\x1B\x25\x01");
}

// graphic_ar: simple array of bytes, 5 bytes per character.
// FIXME: Assumes each codepoint_ar item is 1 higher than the previous.
function sendCustomCharacters(codepoint_ar, graphics_ar) {
	var len = codepoint_ar.length;

	var str = "\x1B\x26\x01" + chr(codepoint_ar[0]) + chr(codepoint_ar.pop());

	for (var i = 0; i < len; i++) {
		str += chr(5);	// 5-line graphics format
		for (var j = 0; j < 5; j++) {
			str += chr(graphics_ar[i * 5 + j] & 0x7f);
		}
	}

	port.write(str);
}

function initClockCharacters(params) {
	var cur_codepoint = INITIAL_CODEPOINT;
	var codepoint_ar = [];
	var graphics_ar = [];

	var fileData = fs.readFileSync(FONT_SHAPE_FILE);
	readImage(fileData, function(err, img){
		if (err) {
			console.log("** readImage error:", err, img);

		} else {
			var width = img.width;
			var height = img.height;
			var data = img.frames[0].data;

			// Prepare aStarGrid
			for (var i = 0; i <= width - 4; i++) {
				var col = new Int8Array(height - 6).fill(NO_GLYPH);
				aStarGrid.push(col);
			}

			function isPixelSet(x, y) {
				if ((x < 0) || (y < 0)) return false;
				if ((x >= width) || (y >= height)) return false;

				// Check the red channel of this pixel
				return data[(y * width + x) * 4] >= 128;
			}

			function createGraphicFromData(x, y) {
				// If there's already a graphic at this x,y: don't remake it.
				var found_codepoint = codepointAtMapLoc(x, y);
				if (found_codepoint) return;

				// Mark this position on aStarGrid, we can move here.
				aStarGrid[x][y] = A_STAR_NOT_FILLED;

				var bytes = [];	// Will be an array of 5 bytes

				for (var i = 0; i < 5; i++) {	// For each column...
					var n = parseInt(0);

					for (var j = 0; j < 7; j++) {	// For each pixel in the column...
						n = (n << 1) | (isPixelSet(x + i, y + j) ? 1 : 0);
					}

					bytes.push(n);
				}

				// If these bytes are identical to another character graphic:
				// Use its codepoint.
				function areBytesIdentical(m1, m2) {
					for (var i = 0; i < 5; i++) {
						if (m1.bytes[i] !== m2.bytes[i]) return false;
					}
					return true;
				}

				for (var m = 0; m < imgMap.length; m++) {
					var obj = imgMap[m];
					if (areBytesIdentical(obj, {bytes:bytes})) {
						imgMap.push({x:x, y:y, codepoint:obj.codepoint, bytes:bytes});
						return;
					}
				}

				// This is a new graphic.
				codepoint_ar.push(cur_codepoint);
				graphics_ar = graphics_ar.concat(bytes);
				imgMap.push({x:x, y:y, codepoint:cur_codepoint, bytes:bytes});

				cur_codepoint++;
				if (cur_codepoint >= 0x80) {
					console.log("** cur_codepoint is", cur_codepoint, "-- may be too high for display to support.", bytes);
				}
			}

			function createGraphicsAlongLine(x, y, xMax, yMax) {
				if (xMax !== null) {
					for (var i = x; i <= xMax; i++) {
						createGraphicFromData(i, y);
					}
				} else if (yMax !== null) {
					for (var j = y; j <= yMax; j++) {
						createGraphicFromData(x, j);
					}
				}
			}

			// Blinky dots
			createGraphicFromData(0, 0);
			createGraphicFromData(19, 0);
			createGraphicFromData(24, 0);
			createGraphicFromData(19, 7);
			createGraphicFromData(24, 7);
			createGraphicFromData(24, 26);

			createGraphicsAlongLine(0, 14, 14, null);	// (tl) heading right
			createGraphicsAlongLine(7, 14, null, 19);	// TR, include "tr" corner
			createGraphicsAlongLine(12, 7, null, 21);	// TR features, down through Z
			createGraphicsAlongLine(12, 16, 24, null);	// heading right, for BL features
			createGraphicsAlongLine(14, 14, null, 16);	// BL to blank
			createGraphicsAlongLine(5, 9, null, 28);	// (tl)_b down to BR features
			createGraphicsAlongLine(0, 28, 14, null);	// BR features
			createGraphicsAlongLine(2, 26, null, 28);	// BR: "r"
			createGraphicsAlongLine(0, 26, 2, 28);	// BR to blank

			sendCustomCharacters(codepoint_ar, graphics_ar);

			// Define numbers
			defineDigit(0, ["(tl)", "(tr)", "(bl)", "(br)"]);
			defineDigit(1, [" ", "l", " ", "l"]);
			defineDigit(2, ["t", "(tr)", "(tl)_b", "="]);
			defineDigit(3, ["t", "z", "(bl)", "(br)"]);
			defineDigit(4, ["l", "l", "t", "tl"]);
			defineDigit(5, ["[", "=", "b", "(br)"]);
			defineDigit(6, ["(tl)_b", "=", "bl", "(br)"]);
			defineDigit(7, ["t", "tr", " ", "r"]);
			defineDigit(8, ["[", "]", "bl", "br"]);
			defineDigit(9, ["[", "]", "b", "(br)"]);
			console.log("defineDigits complete:", digitFeatures);

			params['callback']();
		}
	});
}

//
//  PER FRAME
//

// Construct a message with minimal size; send to VFD to change only the
// modified characters.
function updateDisplayWithFrame(new_frame) {
	var msg = "";

	function isDirty(c) {
		return cur_frame.substr(c,1) !== new_frame.substr(c,1);
	}

	var init_pos = cursor_pos;

	for (var i = 0; i < CHARS_TOTAL; i++) {
		var pos = init_pos + i;
		var pos_mod = pos % CHARS_TOTAL;

		if (isDirty(pos_mod)) {
			// If the cursor is too far away: Jump to this position
			var cursor_distance = ((pos - cursor_pos) % CHARS_TOTAL);

			if (cursor_distance > 3) {
				msg += "\x1F\x24" + chr((pos_mod % CHARS_ACROSS) + 1) + chr((pos_mod / CHARS_ACROSS) + 1);

			} else {
				// Cursor is close by! Catch up by sending the same characters.
				var toCopy = cursor_distance;
				if (toCopy) {
					// 2 x append new_frame, in case copying characters wraps around
					msg += (new_frame + new_frame).substr(cursor_pos, toCopy);
				}
			}

			// Write the dirty character
			msg += new_frame.substr(pos_mod, 1);

			cursor_pos = (pos + 1) % CHARS_TOTAL;	// Cursor is now sitting on the next character.
		}
	}

	if (msg.length) {
		port.write(msg);
	}

	cur_frame = new_frame;
}

var lastSS = "";
function advanceTime(){
	// Get the time
	var now = new Date();

	var ss = ("00" + now.getSeconds()).substr(-2);
	if (ss === lastSS) return;	// No update yet

	var mm = ("00" + now.getMinutes()).substr(-2);
	var hh = ("  " + now.getHours()).substr(-2);
	var time = hh + mm + ss;

	var top = [" "];
	var bottom = [" "];

	for (var i = 0; i < 6; i++) {
		var n = time.charCodeAt(i) - ord('0');

		var digit = digitFeatures[n];

		top = top.concat(digit.slice(0,2));
		bottom = bottom.concat(digit.slice(2,4));

		// Colon separators
		if ((i == 1) || (i == 3)) {
			top = top.concat(" ", ".", " ");
			bottom = bottom.concat(" ", ".", " ");
		}
	}

	function pad(ar) {
		while (ar.length < CHARS_ACROSS) {
			ar.push(" ");
		}
	}
	pad(top);
	pad(bottom);

	var features = top.concat(bottom);

	// Any characters that are changing: Create a tween
	for (var c = 0; c < CHARS_TOTAL; c++) {
		if (dest_features[c] !== features[c]) {
			tween_codepoints[c] = codepointsForAnimation(dest_features[c], features[c]);
			//console.log("tween_codepoints:", tween_codepoints[c].join(","));
			dest_features[c] = features[c];

			// Persist tweenable object in memory
			var obj = {v: 0.0};
			tween_objs[c] = obj;

			// Delays: Animation starts on right-hand side, cascades leftward
			var leftAmt = 1.0 - ((c % CHARS_ACROSS) / CHARS_ACROSS);
			var delay = leftAmt * 1.09;

			var duration = randRange(0.34, 0.41) * (1.0 + leftAmt * 1.25);
			var ease = Quad.easeInOut;

			// Right-most digit should move faster
			if ((c % CHARS_ACROSS) >= (CHARS_ACROSS - 3)) {
				duration = randRange(0.25, 0.33) * (1.0 + leftAmt * 3.2);
				ease = Sine.easeInOut;
			}

			TweenMax.to(
				obj,
				duration,
				{v: tween_codepoints[c].length - 1, rounded: true, onUpdate: tweenCharUpdate, onUpdateParams: [c], ease: ease, delay: delay}
			);
		}
	}
}

function onFrame(){
	advanceTime();

	updateDisplayWithFrame(new_frame);

	setTimeout(onFrame, 1000 / 300);
}

//
//  START
//

port = new SerialPort("/dev/cu.usbserial", {
  baudRate: 9600
}, onPortOpen);

function onPortOpen() {
	setBrightness(1);

	clearDisplay();

	initClockCharacters({callback: onClockCharactersReady});
}

function onClockCharactersReady() {
	enableCustomCharacters();

	// After all data is written to port: Start animation.
	port.drain(portInitComplete);
}

function portInitComplete() {
	for (var d = 0; d < DOT_COUNT; d++) {
		tweenDotComplete(d);
	}

	onFrame();
}
